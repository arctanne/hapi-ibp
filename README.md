# README #

This is IBP Twitter api

### What is this repository for? ###

* Hapi server to consume twitter feed.
* 1.0.0

### How do I get set up? ###

* npm install
* npm run start
* Documentation : http://hapi-twitter-hapi-ibp.193b.starter-ca-central-1.openshiftapps.com/api/v1/documentation
