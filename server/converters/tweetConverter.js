'use strict';

const TweetEntity = require('server/models/TweetEntity');
const UserEntity = require('server/models/UserEntity');

module.exports = tweetConverter();

/**
 * Converter to create Tweet entity from twitter response.
 *
 * @returns tweetConverter.
 */
function tweetConverter() {

  var tweetConverter = {
    getListTweet: getListTweet
  };

  return tweetConverter;


  /**
   * Convert twitter response to tweetEntity array.
   *
   * @param {any} data
   * @returns TweetEntity[]
   */
  function getListTweet(data) {
    let statuses = data.statuses;
    let toReturn = [];
    for (let i = 0, length = statuses.length; i < length; i += 1) {
      toReturn.push(_createTweetItem(statuses[i]));
    }
    return toReturn;
  }


  /**
   * Create TweetEntity.
   *
   * @param {any} data
   * @returns TweetEntity
   */
  function _createTweetItem(data) {
    let tweetObject = new TweetEntity();
    tweetObject.full_text = data.full_text;
    tweetObject.created_at = data.created_at;
    tweetObject.display_text_range = data.display_text_range;
    tweetObject.retweet_count = data.retweet_count;
    tweetObject.favorite_count = data.favorite_count;
    tweetObject.user = _createUserItem(data.user);
    tweetObject.hashtags = _getListHashTag(data.entities.hashtags);
    return tweetObject;
  }


  /**
   * Create UserEntity for TweetEntity.
   *
   * @param {any} data
   * @returns UserEntity
   */
  function _createUserItem(data) {
    let userObject = new UserEntity();
    userObject.name = data.name;
    userObject.screen_name = data.screen_name;
    userObject.profile_image_url = data.profile_image_url;
    userObject.profile_banner_url = data.profile_banner_url;
    return userObject;
  }


  /**
   * Create HashTag array for TweetEntity.
   *
   * @param {any} data
   * @returns string[]
   */
  function _getListHashTag(data) {
    let hashTagArray = [];
    for (let i = 0, length = data.length; i < length; i += 1) {
      hashTagArray.push(data[i].text);
    }
    return hashTagArray;

  }
}
