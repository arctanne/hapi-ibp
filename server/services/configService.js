'use strict';

const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('package.json');
const TwitterApiPlugin = require('server/plugins/twitterApiPlugin');
const apiOptions = {
  info: {
    title: 'Twitter search API Documentation',
    version: Pack.version,
    contact: {
      name: Pack.author.name,
      email: Pack.author.email
    }
  },
  basePath: '/api/v' + Pack.apiVersion,
  pathPrefixSize: 2
};

module.exports = configService();

/**
 * ConfigService to initialize hapi.
 *
 * @returns configService.
 */
function configService() {
  const SERVER_OPTIONS = {
    routes: {
      prefix: apiOptions.basePath
    }
  };
  const service = {
    initConnection: initConnection,
    getPlugins: getPlugins,
    SERVER_OPTIONS: SERVER_OPTIONS
  };

  return service;



  /**
   * Initialize te hapi connection.
   *
   * @param {any} server
   */
  function initConnection(server) {
    const port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
    const ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
    server.connection({ port: port, host: ip, routes: { cors: true } });
  }


  /**
   * Get plugins.
   *
   * @returns Plugins.
   */
  function getPlugins() {
    const serverPlugins = [
      Inert,
      Vision,
      {
        'register': HapiSwagger,
        'options': apiOptions
      },
      TwitterApiPlugin
    ];

    return serverPlugins;
  }


}
