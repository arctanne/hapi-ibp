'use strict';

const twitterApiController = require('./controllers/twitterApiController');
const Joi = require('joi');
Joi.authHeader = require('joi-authorization-header')(Joi);

module.exports.register = pluginRegistration;
module.exports.register.attributes = {
  pkg: require('./package.json')
};

/**
 * Register the twitterApiPlugin.
 *
 * @param {any} server
 * @param {any} options
 * @param {any} next
 */
function pluginRegistration(server, options, next) {

  server.route([{
    method: 'GET',
    path: '/twitterAuthToken',
    handler: twitterApiController.getAuthToken,
    config: {
      description: 'Get an authentication token to cosume twitter API.',
      notes: 'Returns an authentication token require for other services.',
      tags: ['api'],
      response: {
        schema: Joi.object({
          token_type: Joi.string().equal('bearer').required(),
          access_token: Joi.string().required()
        })
      }
    }
  },
  {
    method: 'GET',
    path: '/lastTweets',
    handler: twitterApiController.getLastTweets,
    config: {
      description: 'Get list of last tweets.',
      notes: 'Returns a list of last tweets.',
      tags: ['api'],
      validate: {
        query: {
          account: Joi.string().alphanum().required().description('Ex : "BanquePopulaire". String / Alphanum '),
          count: Joi.number().integer().min(1).required().description('Number of tweets. Ex : "10". Integer')
        },
        headers: Joi.object({
          Authorization: Joi.authHeader(1, 255).description('Ex : "bearer AAAAAA...uD5iFA6Y7A8j". String / Contain bearer')
        }).options({ allowUnknown: true })
      },
      response: {
        schema: Joi.array().items(Joi.object({
          full_text: Joi.string().required(),
          created_at: Joi.date().required(),
          display_text_range: Joi.array().items(Joi.number().integer()),
          retweet_count: Joi.number().integer().min(0).required(),
          favorite_count: Joi.number().integer().min(0).required(),
          user: Joi.object({
            name: Joi.string().required(),
            screen_name: Joi.string().required(),
            profile_image_url: Joi.string().uri().required(),
            profile_banner_url: Joi.string().uri().required()
          }),
          hashtags: Joi.array().items(Joi.string())
        }))
      }
    }
  }]);

  next();
}
