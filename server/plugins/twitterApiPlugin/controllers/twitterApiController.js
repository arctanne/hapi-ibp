'use strict';

const twitterApiConfig = require('server/services/twitterApiConfig');
const Wreck = require('wreck');
const tweetConverter = require('server/converters/tweetConverter');

module.exports = twitterApiController();
const twitterWreck = Wreck.defaults({
  baseUrl: twitterApiConfig.baseUrl,
  json: true
});

/**
 * twitterApiController.
 *
 * @returns controller.
 */
function twitterApiController() {
  const controller = {
    getAuthToken: getAuthToken,
    getLastTweets: getLastTweets
  };

  return controller;

  /**
   * Get the auth token.
   *
   * @param {any} request
   * @param {any} reply
   */
  function getAuthToken(request, reply) {
    twitterWreck.post('oauth2/token', {
      headers: {
        'Authorization': twitterApiConfig.AUTHORIZATION,
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      payload: 'grant_type=client_credentials'
    }).then((data) => {
      reply.response(data.payload);
    }, reply);
  }


  /**
   * Get last tweets of the desire account.
   *
   * @param {any} request
   * @param {any} reply
   */
  function getLastTweets(request, reply) {
    const account = request.query.account;
    const count = request.query.count;
    const authorization = request.headers.authorization;
    twitterWreck.get('/1.1/search/tweets.json?q=from:' + account + '&count=' + count + '&tweet_mode=extended', {
      headers: {
        'Authorization': authorization
      }
    }).then((data) => {
      reply.response(tweetConverter.getListTweet(data.payload));
    }, reply);
  }
}
