'use strict';

/**
 * TweetEntity.
 *
 * @class TweetEntity
 */
class TweetEntity {
  /**
   * Creates an instance of TweetEntity.
   * @memberof TweetEntity
   */
  constructor() {
    this.full_text = '';
    this.created_at = '';
    this.display_text_range = [];
    this.retweet_count = 0;
    this.favorite_count = 0;
    this.user = undefined;
    this.hashtags = [];
  }
}

module.exports = TweetEntity;
