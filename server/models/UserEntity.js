'use strict';

/**
 * User.
 *
 * @class UserEntity
 */
class UserEntity {
  /**
   * Creates an instance of UserEntity.
   * @memberof UserEntity
   */
  constructor() {
    this.name = '';
    this.screen_name = '';
    this.profile_image_url = '';
    this.profile_banner_url = '';
  }
}

module.exports = UserEntity;
