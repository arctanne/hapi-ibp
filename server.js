'use strict';

require('app-module-path').addPath(__dirname);
const configService = require('server/services/configService.js');
const Hapi = require('hapi');
const server = new Hapi.Server();

configService.initConnection(server);


server.register(configService.getPlugins(), configService.SERVER_OPTIONS, (err) => {
  if (err) {
    console.error('Failed to load plugin:', err);
  }

  server.start((err) => {
    if (err) {
      throw err;
    }
    console.log(`Server running at : ${server.info.uri}`);
  });
});
